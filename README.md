# RetroProspectiveCTR

**Description**

This is the analysis for the "Retroactively Prospective Clinical Trial Registration" project by Martin Holst and Benjamin G. Carlisle.

The manuscript preprint, entitled 'Trials that turn from retrospectively registered to prospectively registered: A cohort study of "retroactively prospective" clinical trial registration using history data.', can be found [here](https://www.medrxiv.org/content/10.1101/2022.10.25.22281505v1).

For reproducibility, all datasets that are based on internet downloads are saved in our [OSF Repository](https://osf.io/82vfn/). If you save the files in a 'data' folder within the repository, the analyses should be fully reproducible.

**Software**

R (Version 4.1.3)\
R Package: cthist (Versions 1.1.0)\
R Package: tibble (Version 3.1.6)\
R Package: tidyr (Version 1.2.0)\
R Package: readr (Version 2.1.2)\
R Package: dplyr (Version 1.0.8)\
R Package: ggplot2 (Version 3.3.5)\
R Package: lubridate (Version 1.8.0)\
R Package: testthat (Version 3.1.2)\
R Package: roadoi (Version 0.7.2)\
R Package: xml2 (Version 1.3.3)\
R Package: httr (Version 1.4.2)\
R Package: jsonlite (Version 1.8.0)
