library(tidyverse)
library(roadoi)
library(testthat)

set.seed(2111)


## READ DATA -------------------------------------------------------------------

## read in the data file generated in script 5
versions_short <- read_csv(
  'data/2024-02-25-retpro-processed_versions_short_v.csv'
)
versions_ratings <- read_csv(
  'data/2024-02-25-retpro-processed_versions_ratings.csv'
)


## ADDITIONAL ANALYSIS 1 (Reporting I) -----------------------------------------

## Objective:
## We want to find out how many of the 'retroactively prospective' trials
## actually report a change to their start date in the publication.

## To do this, let's first do some counting, and then calculate the proportion
## of 'retroactively prospective' trials that report a change to the start date.
AA1_n_retpro_trials_reporting <- versions_ratings %>%
  filter(
    (arm_original == 'retpro' | arm_new == 'retpro') &
    change_mentioned == 'yes'
  ) %>%
  nrow()
n_published_ret_pro_trials <- versions_ratings %>%
  filter(
    (arm_original == 'retpro' | arm_new == 'retpro')
  ) %>%
  nrow()
AA1_prop_retpro_trials_reporting <- round(
  (AA1_n_retpro_trials_reporting / n_published_ret_pro_trials)*100, 2
)

# RESULT: None of the 'retroactively prospective' trial report a change to their
# start date.


## ADDITIONAL ANALYSIS 2 (Reporting II) ----------------------------------------

## Objective:
## Additionally, we want to find out whether 'retroactively prospective' trials
## are less likely to report a change to their start date, compared to other
## trials that have changed their start date (but are not 'retroactively
## prospective').

## To do this, we we define a variable that indicates whether a study start date
## was actually changed (this group, of course, includes all 'retroactively
## prospective' trials, but also some of the comparators).
versions_ratings <- versions_ratings %>%
  mutate(
    has_startdate_change = case_when(
      # we depend the determination on how the dates are rounded, again
      startatlaunch_precision == 'day' & startat5a_precision == 'day' ~ startatlaunch != startat5a,
      startatlaunch_precision == 'month' & startat5a_precision == 'day' ~ startatlaunch != startat5a_round,
      startatlaunch_precision == 'month' & startat5a_precision == 'day' ~ startatlaunch_round != startat5a,
      startatlaunch_precision == 'month' & startat5a_precision == 'month' ~ startatlaunch != startat5a,
      TRUE ~ NA
    )
  )

## Then, we calculate the proportions of trials mentioning a change to their
## start date, with the number of trials with changes to their start date as the
## denominator.
## The proportions will be compared, again, by a *z*-test implemented by
## `prop.test` in *R*.
AA2_n_compar_reporting <- versions_ratings %>%
  filter(
    (arm_original == 'compar' | arm_new == 'compar') &
    has_startdate_change == TRUE &
    change_mentioned == 'yes') %>%
  nrow()
n_published_comparators <- versions_ratings %>%
  filter(
    (arm_original == 'compar' | arm_new == 'compar') &
    has_startdate_change == TRUE
  ) %>%
  nrow()
prop.test(
  c(AA1_n_retpro_trials_reporting, AA2_n_compar_reporting),
  c(n_published_ret_pro_trials, n_published_comparators)
)

# RESULT: Test throws out an error, as none of the trials whose publications we
# rated ever mentioned a change to their start date. (Not really worthwhile to
# report this in the paper.)


## ADDITIONAL ANALYSIS 3 (Reporting III) ---------------------------------------

## Objective:
## We want to find out how many of the 'retroactively prospective' do the
## opposite of honestly reporting their start date, by explicitly calling their
## trial 'prospective' when it is not.
AA3_n_retpro_reported_prospective <- versions_ratings %>%
  filter(
    (arm_original == 'retpro' | arm_new == 'retpro') &
    prospective_statement == 'yes'
  ) %>%
  nrow()
AA3_prop_retpro_reported_prospective <- round(
  (AA3_n_retpro_reported_prospective / n_published_ret_pro_trials)*100, 2
)

# RESULT: 12 'retroactively prospective' trials (10.62%) explicitly state that
# they are prospectively registered.


## ADDITIONAL ANALYSIS 4a (Figure 2) -------------------------------------------

## Objective:
## To describe the distribution of 'distances'
## a) between the 5-year start date and the first registration date (i.e., how
##    'far' are the trials prospective in the end? - x-axis), and
## b) between the original (post-launch) start date and the first registration
##    date (i.e., how 'far' were the trials originally retrospective? - y-axis).

## As Figure 2, we describe the dynamic just for the published trials that were
## rated by humans.

## first, we create the necessary variable for the ratings dataset with just the
## published trials
versions_ratings <- versions_ratings %>%
  mutate(
    timediff_at_start = case_when(
      startatlaunch_precision == 'day' ~ startatlaunch - firstreg,
      startatlaunch_precision == 'month' ~ startatlaunch_round - firstreg_round,
      TRUE ~ startatlaunch - firstreg
    )
  ) %>%
  mutate(
    timediff_at_5a = case_when(
      startat5a_precision == 'day' ~ startat5a - firstreg,
      startat5a_precision == 'month' ~ startat5a_round - firstreg_round,
      TRUE ~ startat5a - firstreg
    )
  )
versions_ratings <- versions_ratings %>%
  mutate(
    registration_statement = case_when(
      prospective_statement == 'no' ~ 'no statement',
      prospective_statement == 'retro' ~ 'retrospective',
      prospective_statement == 'yes' ~ 'prospective',
      TRUE ~ 'unknown'
    )
)

## then, do the plot
Figure_2 <- ggplot(versions_ratings) +
  aes(
    as.numeric(timediff_at_5a),
    as.numeric(timediff_at_start) #,
    # color = registration_statement
  ) +
  geom_point(size=0.5) +
  scale_x_continuous(
    lim = c(-1000, 1000)
  ) +
  scale_y_continuous(
    lim = c(-1000, 1000)
  ) +
  xlab("\u2190 Days retrospective at 5 years | Days prospective at 5 years \u2192  ") +
  ylab("\u2190 Days retrospective at launch | Days prospective at launch \u2192  ") +
  geom_vline(xintercept = -21, linetype = 'dotted') +
  annotate(
    "rect",
    xmin = 0, xmax = 1000, ymin = 0, ymax = -1000,
    alpha = 0.1, fill = "red"
  ) +
  annotate(
    "rect",
    xmin = 0, xmax = -1000, ymin = 0, ymax = -1000,
    alpha = 0.1, fill = "yellow"
  ) +
  annotate(
    "rect",
    xmin = 0, xmax = 1000, ymin = 0, ymax = 1000,
    alpha = 0.1, fill = "green"
  ) +
  annotate(
    geom = 'label',
    x = 500, y = -900,
    label = 'Retroactively Prospective Trials (113)',
    color = 'red'
  ) +
  annotate(
    geom = 'label',
    x = 500, y = 900,
    label = 'Trials that stay Prospective (82)'
  ) +
  annotate(
    geom = 'label',
    x = -500, y = -900,
    label = 'Trials that stay Retrospective (93)'
  ) +
  annotate(
    geom = 'label',
    x = -500, y = 900,
    label = 'Retroactively Retrospective Trials (2)'
  )
Figure_2
ggsave('Figure_2.jpeg')


## ADDITIONAL ANALYSIS 4b (Figure S2) ------------------------------------------

## Objective:
## As Figure S2, we run Figure 2, but not with only the 'human-rated' trials,
## but instead with all trials in the sample.

## again, we create the necessary variable for the dataset
versions_short <- versions_short %>%
  mutate(
    timediff_at_start = case_when(
      startatlaunch_precision == 'day' ~ startatlaunch - firstreg,
      startatlaunch_precision == 'month' ~ startatlaunch_round - firstreg_round,
      TRUE ~ startatlaunch - firstreg
    )
  ) %>%
  mutate(
    timediff_at_5a = case_when(
      startat5a_precision == 'day' ~ startat5a - firstreg,
      startat5a_precision == 'month' ~ startat5a_round - firstreg_round,
      TRUE ~ startat5a - firstreg
    )
  )

## test whether we now get the same amount of retroactively prospective trials
test_that(
  'Test whether we now get the same amount of retroactively prospective trials!',
  expect_equal(
    nrow(filter(versions_short, timediff_at_start < 0 & timediff_at_5a >= 0)),
    235
  )
) # it's 235 trials, which is the correct number!

# do the plot
Figure_S2<- ggplot(versions_short) +
  aes(
    as.numeric(timediff_at_5a),
    as.numeric(timediff_at_start)
  ) +
  geom_point(
    size=0.5
  ) +
  scale_x_continuous(
    lim = c(-1000, 1000)
  ) +
  scale_y_continuous(
    lim = c(-1000, 1000)
  ) +
  xlab("\u2190 Days retrospective at 5 years | Days prospective at 5 years \u2192  ") +
  ylab("\u2190 Days retrospective at launch | Days prospective at launch \u2192  ") +
  geom_vline(xintercept = -21, linetype = 'dotted') +
  annotate(
    "rect",
    xmin = 0, xmax = 1000, ymin = 0, ymax = -1000,
    alpha = 0.1, fill = "red"
  ) +
  annotate(
    "rect",
    xmin = 0, xmax = -1000, ymin = 0, ymax = -1000,
    alpha = 0.1, fill = "yellow"
  ) +
  annotate(
    "rect",
    xmin = 0, xmax = 1000, ymin = 0, ymax = 1000,
    alpha = 0.1, fill = "green"
  ) +
  annotate(
    geom = 'label',
    x = 500, y = -900,
    label = 'Retroactively Prospective Trials (235)',
    color = 'red'
  ) +
  annotate(
    geom = 'label',
    x = 500, y = 900,
    label = 'Trials that stay Prospective (5843)'
  ) +
  annotate(
    geom = 'label',
    x = -500, y = -900,
    label = 'Trials that stay Retrospective (5524)'
  ) +
  annotate(
    geom = 'label',
    x = -500, y = 900,
    label = 'Retroactively Retrospective Trials (289)'
  )
Figure_S2
ggsave('Figure_S2.jpeg')


## ADDITIONAL ANALYSIS 5 (Comparisons I) -----------------------------------------

## Objective:
## Based on comments that were made at WCRI, I had the idea to formally assess
## the 'symmetry' of start date changes - i.e., are they equally distributed
## around the registration date, as should be in the case of honest mistakes, or
## biased towards pushing the start date forward?

## In trials that are prospectively registered, we actually expect a 'positive
## bias, as it probably is more common that trials push forward their start date
## if they have not started yet.
## However, for trials that are retrospectively registered, if that trial
## changes its start date after study launch, and it is just a honest mistake,
## then the difference of startat5a - startatlaunch should be symmetrical and
## centered around zero. Mean and Median should also be about Zero.
versions_short_AA5 <- versions_short %>%
  mutate(
    has_startdate_change = if_else(
      startat5a - startatlaunch != 0,
      TRUE,
      FALSE
    )
  ) %>%
  filter(
    has_startdate_change == TRUE &
    (ret_pro_trial == TRUE | always_retrospectively_reg == TRUE)
  ) %>%
  mutate(
    startdate_diff = as.numeric(startat5a - startatlaunch)
  )
# positive values mean the start dates get pushed forward

## do a histogram, and report the mean and median
hist(
  versions_short_AA5$startdate_diff,
  xlim = c(-200, 200),
  breaks = 1000
)
mean(versions_short_AA5$startdate_diff, na.rm = TRUE)
median(versions_short_AA5$startdate_diff, na.rm = TRUE)

rm(versions_short_AA5)

# RESULT: There seems to be a 'positive bias' in start date changes, with those
# retrospectively registered trials that change their start date, they push the
# start date forward with a mean of 72.4 days, and a median of 29 days.


## ADDITIONAL ANALYSIS 6 (Comparisons II) --------------------------------------

## Objective:
## Based on comments that were made at WCRI, I had the idea to assess the
## proximity of start date changes to the publication date, to determine whether
## 'retroactively prospective' trial registration is somehow linked to being
## closer to publication.

## To do this, let's merge the dois with our versions_ratings dataset!
versions_dois1 <- read_tsv(
  'data/2022-07-12_001312-form_1-refset_1-final.tsv'
) %>%
  select(
    c(
      nctid,
      doi
    )
  )
versions_dois2 <- read_tsv(
  'data/2022-07-12_001259-form_1-refset_4-final.tsv'
) %>%
  select(
    c(
      nctid,
      doi
    )
  )
versions_dois <- bind_rows(versions_dois1, versions_dois2) %>%
  filter(nctid %in% versions_ratings$nctid)
versions_ratings <- versions_ratings %>%
  left_join(versions_dois, by = 'nctid') %>%
  # Unpaywall uses lowercase dois, so we have to transform them to avoid trouble
  mutate(doi = tolower(doi))
rm(versions_dois1, versions_dois2, versions_dois)

# ## Then, let's get the relevant data from Unpaywall!
# versions_Unpaywall <- oadoi_fetch(
#   dois = versions_ratings$doi,
#   email = Sys.getenv('roadoi_email')
#   # reproducibility comment: provide your email here by, for example, running
#   # Sys.setenv(roadoi_email = 'insert_your_email_here')
# )
# one doi not found on Unpaywall: 10.4103/njcp.njcp_633_18

# for reproducibility, the script is based on a locally saved version of the 
# Unpaywall data
versions_Unpaywall <- read_csv('data/data_Unpaywall.csv') %>%
  select(
    c(
      doi,
      is_oa,
      genre,
      oa_status,
      journal_is_oa,
      journal_issn_l,
      journal_name,
      publisher,
      published_date,
      title
    )
  )

## merge the data
versions_ratings <- versions_ratings %>%
  left_join(versions_Unpaywall, by = 'doi')

## interestingly, this is two more lines than before! check out why
duplicate_index <- which(duplicated(versions_ratings$nctid))
duplicate_ids <- versions_ratings$nctid[duplicate_index]
check <- versions_ratings %>%
  filter(nctid %in% duplicate_ids)
# manual check reveals that two NCT-IDs have the same doi attached, which is
# correct (it is an NEJM publication that refers to both registry entries)
# thus, we can just remove the duplicates
versions_ratings <- versions_ratings %>%
  slice(1:272, 274:288, 290:292)
length(unique(versions_ratings$nctid))
# not beautiful, but it works
rm(versions_Unpaywall, check, duplicate_ids, duplicate_index)

## now, we can create a dataset with just the retroactively prospective trials,
## to assess proximity of changes to the publication date
versions_ratings_AA6 <- versions_ratings %>%
  filter(
    ret_pro_trial == TRUE
  ) %>%
  relocate(published_date, .after = ret_pro_changedate) %>%
  mutate(published_date = as.Date(published_date)) %>%
  mutate(proximity = ret_pro_changedate - published_date)

## we want to know:
## (a) how many trials change their start date to 'retroactively prospective'
## only after the trial's publication (if they do, maybe we have the wrong
## publication?)
AA6a <- versions_ratings_AA6 %>%
  filter(ret_pro_changedate > published_date) %>%
  nrow() # 13

## (b) of the trials, that change before publication, how many were changed
## post-completion? (we already partly checked this in SO1)
AA6b <- versions_ratings_AA6 %>%
  filter(ret_pro_changedate <= published_date & ret_pro_changedate > completiondate) %>%
  nrow() # 35

## (c) of the trials that change to 'retroactively prospective' before
## publication, how close are they to publication date?
versions_ratings_AA6c <- versions_ratings_AA6 %>%
  filter(ret_pro_changedate > completiondate & ret_pro_changedate <= published_date)
mean(versions_ratings_AA6c$proximity)
median(versions_ratings_AA6c$proximity)

rm(versions_ratings_AA6, versions_ratings_AA6c)

# RESIULT: The distance to publication date seems to be quite large. Which is
# probably not really surprising, as closeness to submission date would probably
# give more info. But we do not have that. (Probably not worthwhile to report
# this in the paper.)


## ADDITIONAL ANALYSIS 7 (Comparisons III) -------------------------------------

## Objective:
## But we might ask ourselves: do the 'changedates' of retpro trials differ from
## ordinary trials that change their start date?
## I.e., are the change dates to 'retroactively prospective' trials more often
## post-completion compared to the *last* start date change of trials that
## are not 'retroactively prospective' (meaning they stay prospective or
## retrospective)?

## first, we have to create the respective variables, and we create them using 
## the 'long' dataset with one row per historical version
versions_long <- read_csv(
  'data/2022-05-04-retpro-processed_versions.csv'
)
temp_1 <- versions_short %>% select(c(nctid, ret_pro_trial))
versions_long <- versions_long %>%
  left_join(temp_1, by = 'nctid')
temp_2a <- versions_long %>%
  filter(ret_pro_trial == TRUE) %>%
  group_by(nctid) %>%
  mutate(changedate = min(version_date[which(retro_pro == 'prospective')])) %>%
  # this is the same variable as in SO2
  # the following three variables are for analysis 8
  mutate(version_start = min(version_number[which(
    overall_status == "Recruiting" |
      overall_status == "Enrolling by invitation" |
      overall_status == "Active, not recruiting" |
      overall_status == "Completed" |
      overall_status == "Terminated"
  )])) %>%
  mutate(version_change = min(version_number[which(retro_pro == 'prospective')])) %>%
  mutate(version_max = max(version_number)) %>%
  slice_head() %>%
  ungroup() %>%
  select(c(nctid, changedate, version_start, version_change, version_max))
temp_2b <- versions_long %>%
  filter(ret_pro_trial == FALSE | is.na(ret_pro_trial)) %>%
  mutate(study_start_date_comparator = NA)
for (i in 2:nrow(temp_2b)) {
  temp_2b$study_start_date_comparator[i] <- temp_2b$study_start_date[i-1]
}
temp_2b <- temp_2b %>%
  relocate(study_start_date_comparator, .after = study_start_date) %>%
  mutate(
    study_start_date_comparator = ifelse(
      version_number == 1,
      NA,
      study_start_date_comparator
    )
  ) %>%
  mutate(
    study_start_date_comparator = as.Date(
      study_start_date_comparator, origin = '1970-01-01'
    )
  )
temp_2b <- temp_2b %>%
  group_by(nctid) %>%
  mutate(
    changedate = max(version_date[which(study_start_date != study_start_date_comparator)], na.rm = TRUE)
  ) %>%
  # the following three variables are for analysis 8
  mutate(version_start = min(version_number[which(
    overall_status == "Recruiting" |
      overall_status == "Enrolling by invitation" |
      overall_status == "Active, not recruiting" |
      overall_status == "Completed" |
      overall_status == "Terminated"
  )])) %>%
  mutate(
    version_change = max(version_number[which(study_start_date != study_start_date_comparator)], na.rm = TRUE)
  ) %>%
  mutate(version_change = na_if(version_change, '-Inf')) %>%
  mutate(version_max = max(version_number)) %>%
  slice_head() %>%
  ungroup() %>%
  select(c(nctid, changedate, version_start, version_change, version_max))
temp_2 <- bind_rows(temp_2a, temp_2b)
test_that(
  'Have we lost any rows?',
  expect_equal(nrow(versions_short), nrow(temp_2))
)
versions_short <- versions_short %>%
  left_join(temp_2, by = 'nctid')
rm(temp_1, temp_2, temp_2a, temp_2b)

## now, we can run the actual analysis
AA7_n_postcomp_change_retpro <- versions_short %>%
  filter(
    ret_pro_trial == TRUE
  ) %>%
  filter(
    completiondate <= changedate
    # see SO1
  ) %>%
  nrow()
AA7_n_postcomp_change_compar <- versions_short %>%
  filter(
    (ret_pro_trial == FALSE | is.na(ret_pro_trial)) & !is.na(changedate)
  ) %>%
  filter(
    always_retrospectively_reg == TRUE
    # (after discussion, we use only retrospectively registered trials with
    # start date changes as a comparator, because for prospectively registered
    # trials, we expect them to change their start date anyway! but for
    # retrospectively registered trials, we do not expect a change - and we
    # assume that if they make a change, it should be more of a mistake that is
    # fixed quickly after trial registration, while 'retroactively prospective'
    # trials on average change later due to malpractice in some...)
  ) %>%
  filter(
    completiondate <= changedate
  ) %>%
  nrow()
AA7_n_retpro <- versions_short %>%
  filter(
    ret_pro_trial == TRUE
  ) %>%
  nrow()
AA7_n_compar <- versions_short %>%
  filter(
    (ret_pro_trial == FALSE | is.na(ret_pro_trial)) & !is.na(changedate)
  ) %>%
  filter(
    always_retrospectively_reg == TRUE
  ) %>%
  nrow()

## run the analysis
prop.test(
  c(AA7_n_postcomp_change_retpro, AA7_n_postcomp_change_compar),
  c(AA7_n_retpro, AA7_n_compar)
)

# RESULT: The proportion of 'retroactively prospective' changing their start
# date post-completion is much higher than in other trials.


## ADDITIONAL ANALYSIS 8 (Comparisons IV) --------------------------------------

## Objective:
## Quantify at which version the 'retroactively prospective' trials change their
## start date form retrospective to prospective.
## The reasoning is that, if these were honest mistakes, the changes would be 
## done early.
versions_AA8 <- versions_short %>%
  filter(ret_pro_trial == TRUE)
mean(versions_AA8$version_change)
median(versions_AA8$version_change)
mean(versions_AA8$version_max)
median(versions_AA8$version_max)
rm(versions_AA8)
# versions_AA8b <- versions_short %>%
#   filter(ret_pro_trial == FALSE)
# mean(versions_AA8b$version_change, na.rm = TRUE)
# median(versions_AA8b$version_change, na.rm = TRUE)
# mean(versions_AA8b$version_max)
# median(versions_AA8b$version_max)

# RESULT: Trials get set from retrospective to prospective quite late in the
# process, with the median being version 4 (of a median of 5 versions overall).


## ADDITIONAL ANALYSIS 9 (Journals) --------------------------------------------

## Objective:
## As an additional analysis, we want to know whether RetPro trials are
## published in ICMJE journals.

ICMJE <- read_csv('data/icmje_journals.csv') %>%
  mutate(journal = tolower(journal))

versions_ratings <- versions_ratings %>%
  mutate(
    journal_name = tolower(journal_name)
  ) %>%
  mutate(
    is_ICMJE_journal = if_else(
      journal_name %in% ICMJE$journal,
      TRUE,
      FALSE
    )
  )
rm(ICMJE)

## manually check for all trials in our manual rating sample of published trials
## (both 'retroactively prospective' and controls): are the unmatched really not
## in the ICMJE list!?
## draw a sample of 20, and manually search the ICMJE list 
versions_sample_checks <- versions_ratings %>%
  filter(is_ICMJE_journal == FALSE)
sample_checks <- sample(versions_sample_checks$journal_name, 20)
rm(versions_sample_checks)
sample_checks
# none of the journals in our sample was found in the ICMJE list - we just
# assume that most matches work well...

## run the actual analysis by calculating the numbers and doing a prop test like
## above
AA8_n_ICMJE_retpro <- versions_ratings %>%
  filter(
    ret_pro_trial == TRUE
  ) %>%
  filter(
    is_ICMJE_journal == TRUE
  ) %>%
  nrow()
AA8_n_ICMJE_compar <- versions_ratings %>%
  filter(
    ret_pro_trial == FALSE | is.na(ret_pro_trial)
  ) %>%
  filter(
    is_ICMJE_journal == TRUE
  ) %>%
  nrow()
AA8_n_retpro_published <- versions_ratings %>%
  filter(
    ret_pro_trial == TRUE
  ) %>%
  nrow()
AA8_n_compar_published <- versions_ratings %>%
  filter(
    ret_pro_trial == FALSE | is.na(ret_pro_trial)
  ) %>%
  nrow()

## run the analysis
prop.test(
  c(AA8_n_ICMJE_retpro, AA8_n_ICMJE_compar),
  c(AA8_n_retpro_published, AA8_n_compar_published)
)

# RESULT: Interestingly, the proportions do not seem to be much different. (Not
# really worthwhile to report this in the paper.)
